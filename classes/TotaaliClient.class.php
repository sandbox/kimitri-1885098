<?php
/**
 * @file
 * A client library for the Fonecta Totaali API.
 * 
 * @author Kimmo Tapala / Verkkojulkaisut Oy <kimmo.tapala@verkkojulkaisut.fi>
 */

class TotaaliClient {
	
	// Class constants
	const HTTP_GET = 'get';
	const HTTP_POST = 'post';
	
	// Cache expiration times in seconds
	const CACHE_PRIMARY_TTL = 300;
	const CACHE_SECONDARY_TTL = 86400;
	const CACHE_AUTHENTICATION_TTL = 3600;
	
	
	
	// Private properties
	private $user, $passwrod, $authEndpoint, $apiEndpoint;
	private $cacheBin = 'cache_totaali';
	
	
	
	// Public properties
	public $reportCacheMiss = FALSE; // Should cache misses be reported
	
	
	
	
	/**
	 * Class constructor.
	 * 
	 * @param string $bearer_token Bearer token used to authenticate requests.
	 * @param string $api_endpoint API endpoint URL. (no trailing slash)
	 */
	public function __construct($user, $password, $auth_endpoint, $api_endpoint){
		$this->user = $user;
		$this->password = $password;
		$this->authEndpoint = $auth_endpoint;
		$this->apiEndpoint = $api_endpoint;
	}
	
	
	/**
	 * Check for cache object staleness and validity
	 *
	 * @param  mixed   $cached Cache object.
	 *
	 * @return boolean
	 *	TRUE if the cached data is expired or the cached data does not exist.
	 */
	public function cacheStale($cached = FALSE){
		if(!$cached || !isset($cached->data) || (isset($cached->expire) && $cached->expire < REQUEST_TIME)){
			return TRUE;
		}
		
		return FALSE;
	}
	
	
	/**
	 * Utility function to report cache misses
	 *
	 * @param string $key Cache object key.
	 */
	public function cacheMiss($key){
		if($this->reportCacheMiss)
			watchdog('Totaali', 'Cache MISS: @key', array('@key' => $key), WATCHDOG_NOTICE);
	}
	
	
	/**
	 * Gets the authentication token.
	 * 
	 * @return string Token.
	 */
	public function getToken(){
		$cacheKey = 'authtoken';
		$cached = cache_get($cacheKey, $this->cacheBin);
		
		$response = (object) array();
		
		if($this->cacheStale($cached)){
			$this->cacheMiss($cacheKey);
			$response = $this->requestToken();
			if($response){
				cache_set($cacheKey, $response, $this->cacheBin, REQUEST_TIME + self::CACHE_AUTHENTICATION_TTL);
			}
		}
		else{
			$response = $cached->data;
		}
		
		return $response;
	}
	
	
	/**
	 * Requests a fresh token from the authentication server.
	 * 
	 * @return string Token string or FALSE on error.
	 */
	private function requestToken(){
		$url = $this->authEndpoint;
		$parameters = array(
			'grant_type' => 'client_credentials',
			'client_id' => $this->user,
			'client_secret' => $this->password
		);
		
		
		// Encode parameters
		$encoded = array();
		foreach ($parameters as $key => $value) {
			$encoded[] = $key.'='.urlencode($value);
		}
		
		$param_data = implode('&', $encoded);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
			'Accept: application/json'
		));
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		
		curl_setopt($ch, CURLOPT_POST, count($parameters));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param_data);
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		
		$responseObject = json_decode($result);
		
		return (!empty($responseObject->access_token)) ? $responseObject->access_token : FALSE;
	}
	
	
	/**
	 * Calls a RESTful API method.
	 * 
	 * This method can be used to bypass the caching layer completely. However,
	 * there should rarely be any need to call this method directly. The
	 * cachedCall() method should be used instead.
	 * 
	 * @param  string $method      API method to call.
	 * @param  array  $parameters  Parameters as an associative array.
	 * @param  string $http_method HTTP method to use. (either 'get' or 'post')
	 * @param  string $path        Path. (used to load entities)
	 * 
	 * @return object              Response object.
	 */
	public function call($method, $parameters, $http_method = 'get', $path = ''){
		$bearerToken = $this->getToken();
		
		$url = $this->apiEndpoint.'/'.$method;
		
		if(!empty($path)){
			$url .= '/'.$path;
		}
		
		$http_method = strtolower($http_method); // Ensure correct case
		
		// Encode parameters
		$encoded = array();
		foreach ($parameters as $key => $value) {
			$encoded[] = $key.'='.urlencode($value);
		}
		
		$param_data = implode('&', $encoded);
		
		
		// Modify URL when using GET
		if($http_method === self::HTTP_GET && !empty($param_data)){
			$url .= '?'.$param_data;
		}
		
		
		// Set up cURL
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$bearerToken 
		));
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		
		if($http_method === self::HTTP_POST && !empty($param_data)){
			curl_setopt($ch, CURLOPT_POST, count($parameters));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $param_data);
		}
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		
		return json_decode($result);
	}
	
	
	/**
	 * Calls a RESTful API method and caches the response.
	 * 
	 * @param  string $method      API method to call.
	 * @param  array  $parameters  Parameters as an associative array.
	 * @param  string $cache_time  Cache time in seconds.
	 * @param  string $http_method HTTP method to use. (either 'get' or 'post')
	 * 
	 * @return object              Response object.
	 */
	public function cachedCall($method, $parameters, $cache_time = 300, $http_method = 'get'){
		$cacheKey = $method.':'.md5(json_encode($parameters));
		$response = (object) array();
		
		$cached = cache_get($cacheKey, $this->cacheBin);
		if($this->cacheStale($cached)){
			$this->cacheMiss($cacheKey);
			$response = $this->call($method, $parameters, $http_method, $path);
			
			cache_set($cacheKey, $response, $this->cacheBin, REQUEST_TIME + $cache_time);
		}
		else{
			$response = $cached->data;
		}
		
		return $response;
	}
	
	
	/**
	 * Loads an entity using the API.
	 * 
	 * @param  string $bundle Entity bundle. ('persons', 'offices' etc.)
	 * @param  string $id     Entity ID.
	 * 
	 * @return object         Response object.
	 */
	public function getEntity($bundle, $id){
		$cacheKey = $bundle.':'.$id;
		$response = (object) array();
		
		$cached = cache_get($cacheKey, $this->cacheBin);
		if($this->cacheStale($cached)){
			$this->cacheMiss($cacheKey);
			$response = $this->call($bundle, array(), self::HTTP_GET, $id);
			
			cache_set($cacheKey, $response, $this->cacheBin, REQUEST_TIME + $cache_time);
		}
		else{
			$response = $cached->data;
		}
		
		return $response;
	}
}

