<?php
/**
 * @file
 * Fonecta Totaali API variables.
 * 
 * @author Kimmo Tapala / Verkkojulkaisut Oy <kimmo.tapala@verkkojulkaisut.fi>
 */


/**
 * Implements hook_variable_group_info()
 *
 * @return array Variable group definitions.
 */
function totaali_variable_group_info(){
	$groups = array();
	
	$groups['totaali_api_config'] = array(
		'title' => t('Totaali API configuration'), 
		'description' => t('Configuration variables of the Fonecta Totaali API module.'), 
		'access' => 'administer site configuration',
		'path' => array('admin/config/search/totaali')
	);

	return $groups;
}


/**
 * Implements hook_variable_info()
 *
 * This function is required to make variables translatable.
 * @see http://api.drupalhelp.net/api/variable/variable.api.php/function/hook_variable_info/7
 *
 * @param  array $options Array of options to build variable properties.
 *
 * @return array          Variable definitions.
 */
function totaali_variable_info($options){
	$variables = array();
	$t_options = array('context' => 'Fonecta Totaali API');
	
	$variables['totaali_user'] = array(
		'type' => 'string',
		'title' => t('Totaali API user', array(), $t_options),
		'default' => '',
		'localize' => FALSE,
		'group' => 'totaali_api_config'
	);
	
	$variables['totaali_password'] = array(
		'type' => 'string',
		'title' => t('Totaali API password', array(), $t_options),
		'default' => '',
		'localize' => FALSE,
		'group' => 'totaali_api_config'
	);
	
	$variables['totaali_authentication_url'] = array(
		'type' => 'string',
		'title' => t('Totaali authentication URL (no trailing slash)', array(), $t_options),
		'default' => 'https://api.fonecta.fi/authentication/token',
		'localize' => FALSE,
		'group' => 'totaali_api_config'
	);
	
	$variables['totaali_api_endpoint'] = array(
		'type' => 'string',
		'title' => t('Totaali API endpoint URL (no trailing slash)', array(), $t_options),
		'default' => 'https://api.omafinder.fi',
		'localize' => TRUE,
		'group' => 'totaali_api_config'
	);
	
	return $variables;
}
